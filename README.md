# README #

Yantram is a tool to implement Infrastructure as a code for ansible and terraform templates.
It comes pre configured with Compute/Network,Storage and Application bundles.
Each repo will contain ansible and terraform components.
It has been divided in 4 modules
1.Public Clouds (AWS,Azure,Google,Alibaba)
2.Private Clouds (VMWare,Openstack)
2.Containers (Kubernates,Openshift)


### What is this repository for? ###

* We will get the best of both ansible and terraform world in this repo.
* We will use terraform to spin up infrastructure and tag them appropriately so that they can be identified dutring ansible run.


### How do I get set up? ###

* Option1 : Use the README.md document to clone the instance and do all the necessary configurations
* Option2: we can spin up a repository using the same tool. Look at the video link here.
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact