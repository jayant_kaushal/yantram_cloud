#!/bin/bash
#
# author: Jayant Kaushal
# site: http://yantram.cloud
# purpose: Create a nginx service in kubernetes cluster



minikube start

#To enable the NGINX Ingress controller, run the following command:

minikube addons enable ingress

#Verify that the NGINX Ingress controller is running

kubectl get pods -n kube-system


kubectl create deployment web --image=gcr.io/google-samples/hello-app:1.0


kubectl expose deployment web --type=NodePort --port=8080

kubectl get service web

minikube service web --url

kubectl apply -f example-ingress.yaml

