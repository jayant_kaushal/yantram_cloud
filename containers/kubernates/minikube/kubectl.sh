#!/bin/bash
#
# author: Jayant Kaushal
# site: http://yantram.cloud
# purpose: Basic kubectl introduction

#list all the nodes running. remember if you are running minicube, there will be only one node which will act both and master and worker node
echo 'Get Help on kubectl \n'
#kubectl -h

echo 'Listing all the Nodes'
echo '#####################################################################################'
kubectl get nodes
echo '#####################################################################################'

#get pods
#echo '\n'
#echo 'Describing node minicube \n'

#kubectl describe node minikube
#kubectl delete node mysql-1601062233-f675f9c4-59nww



#get pods
echo '\n'
echo 'Listing all the pods'
echo '#####################################################################################'
kubectl get pods
echo '#####################################################################################'
echo '\n'
echo 'Listing all the services'
echo '#####################################################################################'
kubectl get services
echo 'Listing all the Deployments'
echo '#####################################################################################'
kubectl get deployment
echo '\n'
echo 'Listing all the Replica Sets \n'
echo '#####################################################################################'
kubectl get replicaset
echo '\n'
echo '#####################################################################################'
echo '\n'
echo 'List all Stateful Sets'
echo '#####################################################################################'
kubectl get statefulset
echo '\n'
echo 'Edit a deployment : #kubectl edit DEPLOYMENT_NAME\n'
#kubectl edit deployment mysql-1600416151
echo '#####################################################################################'

#echo '\n'
#echo 'Debug a kubernetes pod -- log in to application terminal and then look inside \n'
#kubectl logs mysql-1600416151-689d49c5fb-wzh4h





echo '\n'
echo 'Describe a kubernetes pod -- remember it takes a pod to debug , not deployment, not service not anything else \n'
#kubectl describe pod mysql-1600416151-689d49c5fb-wzh4h
echo 'Debug a kubernetes pod -- remember it takes a pod to debug , not deployment, not service not anything else \n'
#kubectl exec -it echo '\n'
echo 'Debug a kubernetes pod -- remember it takes a pod to debug , not deployment, not service not anything else \n'
#kubectl exec  mysql-1600416151-689d49c5fb-wzh4h -- bin/bash -it


echo 'Delete a deployment \n'
#kubectl delete deployment  mysql-1600416151



