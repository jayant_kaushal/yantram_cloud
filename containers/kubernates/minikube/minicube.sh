#!/bin/bash
#
# author: Jayant Kaushal
# site: http://yantram.cloud
# purpose: Create a nginx service in kubernetes cluster



minikube start


#Interact your kubernates cluster:


#get nodes : kubernates

kubectl get nodes

kubectl describe node minikube


#Create deployment from an image:


#kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.10


#Expose deployment in a cluster:


#kubectl expose deployment hello-minikube --type=NodePort --port=8080


#hello-minikube pod is now launched


#get pod info

kubectl get pod

kubectl get service


#get the service of the UR:L


#minikube service hello-minikube --url




#Now when all components are running.
#
#lets delete the service
#
#
#kubectl delete services hello-minikube
#
#
#Delete deployment:
#
#kubectl delete deployment hello-minikube
#
#
#--
#Stop the mincube cluster
#
#minikube stop
#
#
#
#=========================================================== HELM Commans++++++++++++++++++++++++++++++++++++
#GIven that the kubernates cluster is installed and running
#
#
#Add the helm repo on the cluster
#helm repo add stable https://kubernetes-charts.storage.googleapis.com/
#
#
#Install MySQL HelmCHart
#helm install stable/mysql --generate-name
#
#
#
# deploy the dashboard
#kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml

#cat <<EOF | kubectl apply -f -
#apiVersion: v1
#kind: ServiceAccount
#metadata:
#  name: admin-user
#  namespace: kubernetes-dashboard
#EOF
#
#
#
#cat <<EOF | kubectl apply -f -
#apiVersion: rbac.authorization.k8s.io/v1
#kind: ClusterRoleBinding
#metadata:
#  name: admin-user
#roleRef:
#  apiGroup: rbac.authorization.k8s.io
#  kind: ClusterRole
#  name: cluster-admin
#subjects:
#- kind: ServiceAccount
#  name: admin-user
#  namespace: kubernetes-dashboard
#EOF
#
#
#kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
#
#
