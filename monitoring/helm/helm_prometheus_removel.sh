#!/bin/bash
#
# author: Jayant Kaushal
# site: http://yantram.cloud
# purpose: Helm Installation

#curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
#chmod 700 get_helm.sh
#./get_helm.sh




#helm repo add prometheus-com https://prometheus-community.github.io/helm-charts
#helm install prometheus prometheus-com/kube-prometheus-stack --version 9.4.4
helm uninstall prometheus prometheus-com/kube-prometheus-stack

kubectl delete crd prometheuses.monitoring.coreos.com
kubectl delete crd prometheusrules.monitoring.coreos.com
kubectl delete crd servicemonitors.monitoring.coreos.com
kubectl delete crd podmonitors.monitoring.coreos.com
kubectl delete crd alertmanagers.monitoring.coreos.com
kubectl delete crd thanosrulers.monitoring.coreos.com


#helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
#helm repo add stable https://kubernetes-charts.storage.googleapis.com/
#helm repo update

#helm install prometheus prometheus-community/kube-prometheus-stack

