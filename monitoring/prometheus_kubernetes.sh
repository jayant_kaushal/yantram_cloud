#!/bin/bash
#
# author: Jayant Kaushal
# site: http://yantram.cloud
# purpose: Helm Installation


#kubectl get deployment deployment.apps/prometheus-operator-1601063264-kube-state-metrics
echo 'Restart Minikube with prometheus specific configuration'
echo '#####################################################################################'

minikube delete && minikube start --kubernetes-version=v1.19.0 --memory=6g --bootstrapper=kubeadm --extra-config=kubelet.authentication-token-webhook=true --extra-config=kubelet.authorization-mode=Webhook --extra-config=scheduler.address=0.0.0.0 --extra-config=controller-manager.address=0.0.0.0

echo 'Ensure the metrics-server addon is disabled on minikube'
minikube addons disable metrics-server
echo '#####################################################################################'
echo '+'
kubectl get statefulset
echo '#####################################################################################'
echo 'Get info about the prometheus configurations in statefulset/prom.yml'
echo '#####################################################################################'
kubectl get statefulset prometheus-prometheus-operator-160106-prometheus
kubectl describe statefulset prometheus-prometheus-operator-160106-prometheus > statefulset/prom.yml
echo '#####################################################################################'
echo 'Get info about the alertmanager configurations in stateful_set/alert.yml'
echo '#####################################################################################'
kubectl get statefulset alertmanager-prometheus-operator-160106-alertmanager
kubectl describe statefulset alertmanager-prometheus-operator-160106-alertmanager > statefulset/alert.yml
echo '#####################################################################################'
kubectl get deployment
kubectl describe deployment prometheus-operator-160106-operator > deployments/oper.yml
echo '#####################################################################################'

echo '#####################################################################################'
kubectl get configmap
kubectl get secret

echo 'configmap'
echo '#####################################################################################'
#kubectl get secret prometheus-prometheus-operator-160106-prometheus -o yaml > secret/secret.yml
#kubectl get configmap prometheus-prometheus-operator-160106-prometheus-rulefiles-0 -o yaml > configmap/config.yml
kubectl get configmap
echo '#####################################################################################'
echo 'service'
echo '#####################################################################################'
kubectl get service
echo '#####################################################################################'

#kubectl get deployment
#kubectl get pod

#kubectl describe pod mysql-1601236626 > deployments/pod/mysql-1601236626-78c677b65b-hph4f.yml

#kubectl logs prometheus-operator-1601063264-grafana-5bb57f576-6gr77 -c grafana
#kubectl port-forward deployment/prometheus-operator-1601063264-grafana 3000

#kubectl describe deployment prometheus-operator-1601063264-grafana > alert.yml
#kubectl describe deployment prometheus-operator-1601063264-grafana > alert.yml
#kubectl describe deployment prometheus-operator-1601063264-grafana > alert.yml
kubectl --record deployment.apps/nginx-deployment set image deployment.v1.apps/nginx-deployment nginx=nginx:1.16.1


























































































































































kubectl get statefulset prometheus-prometheus-operator-160106-prometheus >> prom.yml

