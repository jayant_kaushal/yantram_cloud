variable "client" {
  type = string
  description = "a default name prefix for all the resources configured under this deployment"
  default = "yantram"
}
variable "gcp_cred_kind_dup" {
  type = string
  default = "serviceaccount"
}
variable "gcp_project_dup" {
  type = string
  default = "YOUR_DEFAULT_PROJECT_NAME"
}
variable "gcp_cred_file_dup" {
  type = string
  default = "LOCATION_FOR_CRED_FILE"
}
variable "public_key_file" {
  type = string
  default = "LOCATION_FOR_PUBLIC_KEY"
}
variable "source_image" {
  type = string
  default = "debian-cloud/debian-9"
}


provider "google" {
  version = "3.39"
  credentials = file(var.gcp_cred_file_dup)
  // credentials = file("~/concise-rampart-288609-3d94f060ecb5.json")
  project = var.gcp_project_dup
  region = "us-central1"
}
// Configure the Google Cloud provider

// Terraform plugin for creating random ids
variable "machine_type" {
  type = string
  default = "n1-standard-1"
}
variable "git_password" {
  type = string
  default = "n1-standard-1"
}
variable "server_tags" {
  type = list(string)
  default = [
    "webserver"]
}
variable "zone" {
  type = string
  default = "us-central1-a"
}
variable "instance_name" {
  type = string
  default = "yantram"
}
//resource "google_compute_network" "default_network" {
//  name = "${var.client}-network"
//}
//resource "google_compute_firewall" "default_firewall" {
//  //  name = var.client+"-firewall"
//  name = "${var.client}-firewall"
//  network = google_compute_network.default_network.name
//
//  allow {
//    protocol = "icmp"
//  }
//
//  allow {
//    protocol = "tcp"
//    ports = [
//      "22",
//      "80",
//      "8080",
//      "1000-2000"]
//  }
//
//  source_tags = var.server_tags
//}


// A single Compute Engine instance
resource "google_compute_instance" "default_instance" {
  name = "${var.client}-instance"
  tags = var.server_tags
  machine_type = var.machine_type
  zone = var.zone
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }
  // Make sure flask is installed on all new instances for later steps
  metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build-essential python-pip rsync; pip install flask"
  metadata = {
    ssh-keys = "yantram:${file(var.public_key_file)}"
  }
  network_interface {
    network = "default"
    access_config {
      // Include this section to give the VM an external ip address
    }
  }
}

